# physify


Computes mass and center of mass for given mesh.
There're functions provided to calculate everything from numpy arrays of
vertices and triangles or from mesh file path (unstable version of pymesh is used).

Full script usage: `physify.py [-h] [-s SCALE] [-d DENSITY] mesh_path`

Example:
```sh
./physify.py -d 1.4 3D_Printer_test_fixed_stl_3rd_gen.STL
Volume: 48701.6117440
Center of mass: [48.2894229, 40.5321398, 7.4043655]
Mass: 68182.2564416
```

Currently (september 2018) in order to run script as-is it is required to install
unstable pymesh version (see https://pymesh.readthedocs.io/en/latest/installation.html)