#!/usr/bin/python3

from numpy import matrix
from numpy import ndarray
from numpy import zeros
from numpy.linalg import det
from numpy import sum

import pymesh

def calculate_mesh_props(
    vertices,
    faces,
    face_count,
    scale = 1.0,
    density=-1.0):
    """
    Calculate volume and center of mass of closed(!) triangle mesh
    Params:
       vertices: ndarray with shape (vertex_count, 3) of mesh vertices
       faces: ndarray with shape (triangle_count, 3) of mesh indices
       face_count: triangle count
       scale: mesh scale
       density: if supplied then mesh mass is calculated
    """
    assert(len(vertices.shape) == 2 and vertices.shape[1] == 3)
    assert(len(faces.shape) == 2 and faces.shape[1] == 3)
    assert(abs(scale) > 0.0000001)
    assert(density != 0.0)

    prepared_vertices = ndarray((face_count, 3, 3), vertices.dtype)
    for i in range(face_count):
        prepared_vertices[i][0] = vertices[faces[i][0]]
        prepared_vertices[i][1] = vertices[faces[i][1]]
        prepared_vertices[i][2] = vertices[faces[i][2]]

    # the volume of tetrahedron is abs(dot(v1-v4, cross(c2-v4, v3-v4))) / 6
    # or abs(det(v1 - v4, v2 - v4, v3 - v4)) / 6
    # we have chose v4 to be (0, 0, 0) so the volume of tetrahedron is:
    # abs(det(v1, v2, v3)).
    # additionally we want to cancel overlapping volumes as some of them
    # (depending on face direction) present outer space
    # which has to be subtructed
    total_volume = 0.0
    total_center_of_mass = zeros((3), dtype=vertices.dtype)
    for i in range(face_count):
        # calculate tetrahedron volume
        tetrahedron_volume = det(prepared_vertices[i]) / 6.0
        total_volume += tetrahedron_volume

        # calculate tetrahedron center of mass
        tetrahedron_mass_center = sum(prepared_vertices[i], 0) / 4.0

        # considering separate tetrahedron centers of mass as particles
        # the total system center of mass is:
        # 1 / total_mass * sum(tetrahedron_i_mass * tetrahedron_i_mass_center).
        # as we suppose that density is uniform then
        # volume can be used instead of mass
        total_center_of_mass += tetrahedron_mass_center * tetrahedron_volume
        # print(total_center_of_mass)
    total_center_of_mass = (total_center_of_mass / total_volume) * scale
    total_volume = abs(total_volume * scale)

    result_dict = {'volume':total_volume, 'center_of_mass':total_center_of_mass}
    if density > 0.0:
       mass = total_volume * density
       result_dict['mass'] = mass
    return result_dict

def calc_model_props_from_pymesh(surface_mesh, scale=1.0, density=-1.0):
    if not surface_mesh.is_closed():
        raise Exception('Only closed meshes are supported.')

    # triangluate mesh if it has quad faces
    if surface_mesh.vertex_per_face == 4:
        surface_mesh = pymesh.quad_to_tri(surface_mesh)
    elif surface_mesh.vertex_per_face != 3:
        raise Exception('Only triangle and quad meshes are supported.')

    result_dict = calculate_mesh_props(
        surface_mesh.vertices,
        surface_mesh.faces,
        len(surface_mesh.faces),
        scale,
        density)

    return result_dict

def calc_model_props_from_file(filename, scale=1.0, density=-1.0):
    surface_mesh = pymesh.load_mesh(filename)
    calc_result = calc_model_props_from_pymesh(surface_mesh, scale, density)
    return calc_result

def print_mesh_props_to_stdout(properties):
    if 'volume' in properties:
        print('Volume: {0:.7f}'.format(properties['volume']))

    if 'center_of_mass' in properties:
        cm = properties['center_of_mass']
        print('Center of mass: [{0:.7f}, {1:.7f}, {2:.7f}]'.format(cm[0], cm[1], cm[2]))

    if 'mass' in properties:
        print('Mass: {0:.7f}'.format(properties['mass']))

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description='Computes mass and center of mass for given mesh.')
    parser.add_argument(
        'mesh_path',
        help='mesh file path; supported formats are: .obj, .off, .ply, .stl, .mesh, .msh',
        type=str)
    parser.add_argument(
        '-s',
        '--scale',
        default=1.0,
        help='mesh scale; can be used to change size of model or to meet the units of specified density',
        type=float)
    parser.add_argument(
        '-d',
        '--density',
        default=-1.0,
        help='density of model material; if specified then the mass of model will be computed',
        type=float)

    args = parser.parse_args()

    mesh_path = args.mesh_path
    scale = args.scale
    density = args.density

    try:
        mesh_props = calc_model_props_from_file(mesh_path, scale, density)
        print_mesh_props_to_stdout(mesh_props)
    except Exception as e:
        print('Error: {}'.format(str(e)))